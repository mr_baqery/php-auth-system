<?php


namespace App\Controllers;


use App\Models\User;
use Illuminate\Http\JsonResponse;
use SSD\DotEnv\DotEnv;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Controller
{
    protected array $rules = [
        "name" => "required",
        "email" => "required|email",
        "password" => "required|confirmed",
        "password_confirmation" => "required"
    ];

    protected function constructor()
    {
        return $this->redirectIfLoggedIn();
    }

    public function index()
    {
        return $this->view('pages.register');
    }

    public function post()
    {
        $this->postRequestOnly();

        try {
            $this->collectRequest([
                "name", "email", "password", "password_confirmation"
            ]);

            $this->validateRequest();

            $this->validateEmail();

            $this->input->offsetUnset('password_confirmation');

            return $this->addUser();

        } catch (\Exception $e) {
            return (new JsonResponse(
                $this->errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ))->sendHeaders()->getContent();
        }

    }

    private function validateEmail()
    {
        $user = User::byEmail($this->input->get('email'))->first();

        if (!is_null($user)) {
            $this->addErrors("email", 'User already exists');
            throw new \Exception('User already exists');
        }

    }

    private function addUser()
    {
        if (!$user = User::create($this->input->toArray())) {
            $this->addErrors('name', 'failed');
            throw new \Exception("Could not add new user record");
        }

        if (DotEnv::is('VERIFICATION', true)) {
            return $this->activation($user);
        }

        $this->guard->login($user);

        return (new JsonResponse([
            'redirect' => '/dashboard'
        ]))->sendHeaders()->getContent();
    }

    public function activation(User $user = null)
    {
        try {
            if (is_null($user)) {
                $user = User::byEmail($this->request->get('email'))->first();
            }

            if (is_null($user)) {
                $this->addErrors('email', 'invalid');
                throw new \Exception("Invalid email address");
            }

            if (!$this->sendActivationEmail($user)) {
                $this->addErrors('email', 'technical');
                throw new \Exception("Email could be not be sent");
            }

            $message = 'Please check your email for instruction on how to activate you account.';

            return (new JsonResponse([
                "message" => $message
            ]))->sendHeaders()->getContent();


        } catch (\Exception $e) {
            return (new JsonResponse(
                $this->errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ))->sendHeaders()->getContent();
        }
    }

    private function sendActivationEmail(User $user)
    {
        $user->resetToken();

        return $this->sendEmail($user);
    }

    private function sendEmail(User $user)
    {
        return $this->container->make('mail')
            ->addFrom(
                DotEnv::get('BUSINESS_EMAIL'),
                DotEnv::get('BUSINESS_NAME'),
            )
            ->addTo($user->email, $user->name)
            ->setSubject('Activate account')
            ->setBody($this->messageBody($user))
            ->send();
    }

    private function messageBody(User $user): string
    {
        $url = $this->request->root() . "/activate?token={$user->token}";

        $out = '<p>Please the link below in order to activate your account</p>';
        $out .= "<a href='{$url}' target='_blank'>{$url}</a>";
        return $out;
    }
}