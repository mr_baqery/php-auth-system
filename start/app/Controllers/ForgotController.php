<?php


namespace App\Controllers;


use App\Models\User;
use Illuminate\Http\JsonResponse;
use SSD\DotEnv\DotEnv;
use Symfony\Component\HttpFoundation\Response;

class ForgotController extends Controller
{
    protected array $rules = [
        'email' => 'required|email'
    ];

    protected function constructor()
    {
        $this->redirectIfLoggedIn();
    }

    public function index()
    {
        return $this->view('pages.forgot');
    }

    public function post()
    {
        $this->postRequestOnly();

        try {
            $this->collectRequest(['email']);

            $this->validateRequest();

            $user = $this->validateEmail();

            if (!$this->sendEmail($user)) {

                $this->addErrors('email', 'technical');
                throw new \Exception("Sending email failed");
            }

            return (new JsonResponse([
                "message" => "Please check your mailbox for instruction on how to reset your password"
            ]))->sendHeaders()->getContent();

        } catch (\Exception $e) {
            return (new JsonResponse(
                $this->errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ))->sendHeaders()->getContent();
        }

    }

    private function validateEmail()
    {
        if (is_null($user = User::byEmail($this->input['email'])->first())) {
            $this->addErrors('email', 'invalid');
            throw new \Exception("Email does not exists");
        }

        return $user;

    }

    private function sendEmail(User $user)
    {
        return $this->container->make('mail')
            ->addFrom(
                DotEnv::get('BUSINESS_EMAIL'),
                DotEnv::get('BUSINESS_NAME')
            )->addTo($user->email, $user->name)
            ->setSubject('Password reset request')
            ->setBody($this->messageBody($user))
            ->send();
    }

    private function messageBody(User $user): string
    {
        $url = $this->request->root() . "/reset?token={$user->resetToken()}";

        $out = '<p>Please click the link below in order to reset your password.</p>';
        $out .= "<a href={$url} target='_blank'>{$url}</a>";

        return $out;
    }

}