<?php

use App\Utilities\Guard;
use App\Utilities\Kernel;
use App\Utilities\Mail\MailManager;
use Illuminate\Container\Container;
use Illuminate\Http\Request;

/**
 * All of this code will display relevant page when we requested through
 * the browser
 */

require_once "../bootstrap/autoload.php";

// we are going to bind instances of certain classes so
// the later we can actually pull them directly from the container
$container = new Container();
$container->instance('request', Request::capture());
$container->instance('guard', new Guard);
$container->bind('mail', function () {
    return MailManager::make();
});

$app = new Kernel($container);

$views = realpath(__DIR__ . '/../resources/views');
$cache = realpath(__DIR__ . '/../resources/cache');

echo $app->make($views, $cache)->render();