<?php


namespace App\Traits;


use Illuminate\Support\Collection;
use JetBrains\PhpStorm\Pure;

trait Binder
{
    protected function bind(Collection $collection)
    {
        if ($collection->isEmpty()) {
            return;
        }
        $collection->filter([$this, 'filterBindings'])
            ->each([$this, 'bindProperties']);
    }

    public function filterBindings($value, $key)
    {
        return property_exists($this, $key);
    }

    public function bindProperties($value, $key)
    {
        $this->{$key} = $value;
    }

}