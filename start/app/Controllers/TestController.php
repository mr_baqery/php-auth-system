<?php


namespace App\Controllers;


class TestController extends Controller
{
    protected function constructor()
    {
        $this->redirectIfNotLoggedIn();
    }

    public function index()
    {
        echo "Auth required page";
    }
}