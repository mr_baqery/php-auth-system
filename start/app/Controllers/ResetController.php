<?php


namespace App\Controllers;


use App\Models\User;
use Illuminate\Http\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ResetController extends Controller
{

    protected array $rules = [
        'password' => 'required|confirmed',
        'password_confirmation' => 'required'
    ];

    public function constructor()
    {
        $this->redirectIfLoggedIn();
    }

    public function index()
    {
        $user = $this->getUser();

        return $this->view('pages.reset')->with('user', $user);
    }

    private function getUser()
    {
        if (is_null($user = $this->getUserByToken())) {
            $this->addErrors('password', 'token');
            throw new \Exception("Invalid request");
        }

        return $user;
    }

    public function post()
    {
        $this->postRequestOnly();

        try {
            $user = $this->getUser();

            $this->collectRequest([
                'password',
                'password_confirmation'
            ]);

            $this->validateRequest();

            $this->input->offsetUnset('password_confirmation');

            $this->updatePassword($user);

            return (new JsonResponse([
                'message' => 'Your password has been updated successfully'
            ]))->sendHeaders()->getContent();

        } catch (\Exception $e) {
            return (new JsonResponse(
                $this->errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ))->sendHeaders()->getContent();
        }

    }

    private function updatePassword(User $user)
    {
        if (!$user->updatePassword($this->input->get('password'))) {
            $this->addErrors('password', 'failed');
            throw new \Exception('Password could not be updated.');
        }
    }
}