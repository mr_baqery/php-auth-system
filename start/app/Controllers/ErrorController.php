<?php


namespace App\Controllers;


use Illuminate\View\Factory;
use Illuminate\View\View;

class ErrorController extends Controller
{

    /**
     * Index controller
     * @param null $message
     * @return Factory|View
     */
    public function index($message = null)
    {
        http_response_code(404);
        return $this->view('pages.error')->with('message' , $message);
    }
}