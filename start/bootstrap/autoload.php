<?php

use App\Utilities\Session\SessionManager;
use SSD\DotEnv\DotEnv;

require_once(realpath(__DIR__ . "/../vendor/autoload.php"));

SessionManager::start();

$dotenv = new DotEnv(realpath(__DIR__ . '/../.env'));
$dotenv->overload();
// if one this var does not exist,
// then required() with throw an exception
$dotenv->required([
    'DB_CONNECTION',
    'SESSION_DRIVER',
    'DB_HOST',
    'DB_DATABASE',
    'DB_USERNAME',
    'DB_PASSWORD'
]);