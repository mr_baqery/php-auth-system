<?php


namespace App\Controllers;


use Illuminate\View\Factory;
use Illuminate\View\View;

class DashboardController extends Controller
{
    public function index(): Factory|View
    {
        $this->authorize();

        return $this->view('pages.dashboard')->with(['user', $this->guard->user()]);
    }

}