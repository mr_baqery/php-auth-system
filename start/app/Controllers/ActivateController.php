<?php


namespace App\Controllers;


use App\Models\User;

class ActivateController extends Controller
{
    public function index()
    {
        $this->activate();

        return $this->view('pages.activate');
    }

    private function activate()
    {
        if (is_null($user = $this->getUserByToken())) {
            throw new \Exception("Invalid request");
        }

        $user->makeActive();

    }


}