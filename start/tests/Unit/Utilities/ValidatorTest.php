<?php


namespace AppTest\Unit\Utilities;


use App\Utilities\Validator\Validator;
use AppTest\BaseCase;
use Illuminate\Support\Collection;

class ValidatorTest extends BaseCase
{
    /**
     * @test
     */
    public function validation_fails_with_input_missing_value()
    {
        $rules = new Collection([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        $input = new Collection([
            'name' => '',
            'email' => '',
            'password' => '',
            'password_confirmation' => ''
        ]);

        $validator = new Validator($rules, $input);

        $this->assertFalse(
            $validator->isValid(),
            "Test gone failed"
        );

        $this->assertSame([
            'name' => ['required'],
            'email' => ['required', 'email'],
            'password' => ['required'],
            'password_confirmation' => ['required']
        ],
            $validator->errors,
            "Validation errors differ with empty input"
        );
    }

    /**
     * @test
     */
    public function validation_fails_with_invalid_email()
    {
        $rules = new Collection([
            'email' => 'required|email'
        ]);

        $input = new Collection([
            'email' => 'some string '
        ]);

        $validator = new Validator($rules, $input);

        $this->assertFalse(
            $validator->isValid(),
            "Validation returned true with request containing invalid email address"
        );

        $this->assertSame(
            [
                'email' => ['email']
            ],
            $validator->errors,
            'Validation errors do not math invalid email address'
        );
    }

    /**
     * @test
     */
    public function validation_fails_with_invalid_confirmation_input()
    {
        $rules = new Collection([
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        $input = new Collection([
            'password' => 'abc',
            'password_confirmation' => 'def'
        ]);

        $validator = new Validator($rules, $input);

        $this->assertFalse(
            $validator->isValid(),
            "Validation returned true with request containing invalid confirmation"
        );

        $this->assertSame(
            [
                'password' => ['confirmed']
            ],
            $validator->errors
        );
    }

    /**
     * @test
     */

    public function validation_passes_with_valid_input()
    {
        $rules = new Collection([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        $input = new Collection([
            'name' => 'set',
            'email' => 'info@info.com',
            'password' => 'sec',
            'password_confirmation' => 'sec'
        ]);

        $validator = new Validator($rules, $input);

        $this->assertTrue(
            $validator->isValid(),
            "Validation returned false with request containing valid confirmation"
        );

        $this->assertSame(
            [],
            $validator->errors
        );
    }
}