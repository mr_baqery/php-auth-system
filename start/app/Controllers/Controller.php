<?php


namespace App\Controllers;


use App\Models\User;
use App\Utilities\Guard;
use App\Utilities\Validator\Validator;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\Factory;
use Illuminate\View\View;
use SSD\Blade\Blade;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class Controller
{
    /**
     * @var Blade
     */
    protected $blade;

    /**
     * @var Container
     */
    protected $container;
    /**
     * @var mixed|object
     */
    protected Guard $guard;
    /**
     * @var mixed|object
     */
    protected $request;

    /**
     * @var Validator
     */
    protected $validator;

    /**
     * @var Collection
     */
    protected $input;

    /**
     * @var array
     */
    protected array $rules = [];

    /**
     * @var array
     */
    protected $errors = [];

    /**
     * Controller constructor.
     * @param Blade $blade
     * @param Container $container
     * @throws BindingResolutionException
     */
    public function __construct(Blade $blade, Container $container)
    {
        $this->blade = $blade;
        $this->container = $container;

        // we are getting instance of guard from where we have
        // initialized it (public/index.php)
        $this->guard = $container->make('guard');
        $this->request = $container->make('request');
        $this->constructor();
    }

    /**
     * Add to parent constructor
     */
    protected function constructor()
    {
    }

    /**
     * Get view instance
     * @param null $view
     * @param array $data
     * @param array $mergeData
     * @return Factory|View
     */
    protected function view($view = null, $data = [], $mergeData = [])
    {
        return $this->blade->view($view, $data, $mergeData);
    }

    /**
     * Authorize request
     *
     * @return RedirectResponse|null
     */
    protected function authorize()
    {
        if ($this->guard->isAuthenticated()) {
            return true;
        }

        return (new RedirectResponse('/'))->send();
    }

    /**
     * Redirect if user is logged in
     *
     * @return RedirectResponse|null
     */
    protected function redirectIfLoggedIn()
    {
        if (!$this->guard->isAuthenticated()) {
            return null;
        }

        return (new RedirectResponse('/dashboard'))->send();
    }

    protected function redirectIfNotLoggedIn()
    {
        if ($this->guard->isAuthenticated()) {
            return null;
        }

        return (new RedirectResponse('/login'))->send();
    }


    /**
     * Check if request method is POST
     *
     * @return bool
     */
    protected function isPost()
    {
        return $this->request->method() === 'POST';
    }

    /**
     * Only allow POST request method
     *
     * @return void
     */
    protected function postRequestOnly()
    {
        if (!$this->isPost()) {
            throw new BadRequestHttpException('Invalid request method!');
        }
    }

    /**
     * Collection request items.
     *
     * @param array $items
     */
    protected function collectRequest(array $items = [])
    {
        $this->input = new Collection(
            $this->request->only($items)
        );
    }

    /**
     * Validate request
     * return @void
     */
    protected function validateRequest()
    {
        $this->validator = new Validator(
            new Collection($this->rules),
            $this->input
        );

        if (!$this->validator->isValid()) {
            $this->errors = array_merge($this->errors, $this->validator->errors);
        }
    }

    /**
     *
     * @param string $key
     * @param string $rule
     */
    protected function addErrors(string $key, string $rule)
    {
        if (array_key_exists($key, $this->errors) &&
            in_array($rule, $this->errors[$key])
        ) {
            return;
        } else {
            $this->errors[$key][] = $rule;
        }
    }

    protected function getUserByToken()
    {
        $token = $this->request->get('token');

        if (empty($token) || is_null($user = User::byToken($token)->first())) {
            return null;
        }

        return $user;
    }
}