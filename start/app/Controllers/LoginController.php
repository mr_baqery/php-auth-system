<?php


namespace App\Controllers;


use App\Models\User;
use Illuminate\Http\JsonResponse;
use SSD\DotEnv\DotEnv;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    protected array $rules = [
        "email" => 'required|email',
        'password' => 'required'
    ];

    public function constructor()
    {
        return $this->redirectIfLoggedIn();
    }

    public function index(): string
    {
        return $this->view('pages.login');
    }

    public function post()
    {

        $this->postRequestOnly();

        try {
            $this->collectRequest([
                'email', 'password', 'remember_me'
            ]);

            $this->validateRequest();

            $this->guard->login(
                $this->verifyUser(),
                $this->input->get('remember_me')
            );

            return (new JsonResponse([
                "redirect" => "/dashboard"
            ]))->sendHeaders()->getContent();

        } catch (\Exception $e) {

            return (new JsonResponse(
                $this->errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            ))->sendHeaders()->getContent();
        }
    }

    private function verifyUser()
    {
        $user = User::byEmail($this->input['email'])->first();

        if (!is_null($user)) {
            return $this->checkUserPassword($user);
        }

        return $this->invalidCredentials();
    }

    private function checkUserPassword(User $user)
    {
        if (!$user->verifyPassword($this->input->get('password'))) {
            return $this->invalidCredentials();
        }

        return $this->checkIfActive($user);
    }

    private function invalidCredentials()
    {
        $this->addErrors('email', 'invalid');
        throw new \Exception('User not found!');
    }

    private function checkIfActive(User $user)
    {
        if (DotEnv::is('VERIFICATION', true) && !$user->isActive()) {
            return $this->inactive();
        }

        return $user;

    }

    private function inactive()
    {
        $this->addErrors('email', 'inactive');
        throw new \Exception("User not active");
    }


}