<?php


namespace App\Utilities\Cookie;


class FileCookie implements Contract
{

    /**
     * Get all cookie
     * @return array
     */
    public function all(): array
    {
        return $_COOKIE;
    }

    /**
     * Check if cookie has a given key
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool
    {
        return isset($_COOKIE[$key]);
    }

    /**
     * Set cookie
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set(string $key, $value, $duration = 0)
    {
        setcookie($key, $value, $duration, '/');
    }

    /**
     * Get cookie
     * @param string $key
     * @return mixed
     */
    public function get(string $key)
    {
        if (!$this->has($key)) {
            return null;
        }

        return $_COOKIE[$key];
    }

    /**
     * @param string $key
     * @return void
     */
    public function remove(string $key)
    {
        setcookie($key, '', time() - 1000, '/');
    }
}