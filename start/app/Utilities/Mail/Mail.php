<?php


namespace App\Utilities\Mail;


use App\Traits\Binder;
use Illuminate\Support\Collection;

abstract class Mail
{

    use Binder;

    /**
     * @var array
     */
    protected $from = [];

    /**
     * @var array
     */
    protected $to = [];

    /**
     * @var string
     */
    protected $subject;

    protected $body;

    protected $html = true;

    /**
     * @var string
     */
    protected $exception;

    public function __construct(array $props = [])
    {
        $this->bind(new Collection($props));
    }


    public function addFrom($email, $name = null)
    {
        $this->emailName($email, $name, $this->from);

        return $this;
    }

    private function emailName($emails, $name = null, &$property)
    {
        if (is_array($emails)) {
            $this->emailNameArray($emails, $property);
            return;
        }
        $name = !is_null($name) ? $name : $emails;
        $property[$emails] = $name;
    }


    private function emailNameArray($emails, &$property)
    {
        foreach ($emails as $email => $name) {
            if (!is_string($email)) {
                $email = $name;
            }

            $property[$email] = $name;
        }
    }

    public function getFrom(): array
    {
        return $this->from;
    }

    public function addTo($emails, $name = null)
    {
        $this->emailName($emails, $name, $this->to);

        return $this;
    }

    public function getTo()
    {
        return $this->to;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    public function getSubject(): string
    {
        return $this->subject;
    }

    public function setBody($body, $html = true)
    {
        $this->body = $body;
        $this->html = $html;

        return $this;
    }

    public function getBody()
    {
        return $this->body;
    }


    protected function html(): ?string
    {
        return $this->html ? "text/html" : null;
    }

    public function reset()
    {
        $this->from = [];
        $this->to = [];
        $this->subject = null;
        $this->body = null;
        $this->html = true;
    }

    protected function validate()
    {
        if (!$this->check()) {
            throw new \Exception("Message is incomplete");
        }
    }

    private function check(): bool
    {
        return (
            !empty($this->from) &&
            !empty($this->to) &&
            !empty($this->subject) &&
            !empty($this->body)
        );
    }

    abstract public function send();


}