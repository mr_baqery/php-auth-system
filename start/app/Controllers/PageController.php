<?php


namespace App\Controllers;


use Illuminate\View\Factory;
use Illuminate\View\View;

class PageController extends Controller
{

    public function constructor()
    {
//        $this->redirectIfNotLoggedIn();
    }

    /**
     * Show page
     * @return Factory|View
     */
    public function index()
    {


        $slug = $this->request->segment(1);

        return $this->view('pages.page')
            ->with('slug', $slug);
    }
}