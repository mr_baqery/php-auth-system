<?php


namespace App\Controllers;


use App\Utilities\Session\SessionManager;
use Illuminate\Http\RedirectResponse;

class LogoutController extends Controller
{
    public function index()
    {
        $this->guard->logout();

        return (new RedirectResponse('/'))->send();
    }
}