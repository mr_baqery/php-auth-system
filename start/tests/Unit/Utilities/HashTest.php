<?php


namespace AppTest\Unit\Utilities;


use App\Utilities\Hash;
use PHPUnit\Framework\TestCase;

class HashTest extends TestCase
{
    public function testCorrectlyMakesAndVerifiesHash()
    {
        $hash = Hash::make('secret');

        $this->assertTrue(
            Hash::verify('secret', $hash),
            'Hash::make verify failed '
        );
    }
}