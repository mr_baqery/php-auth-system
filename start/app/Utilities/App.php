<?php


namespace App\Utilities;

use App\Controllers\Controller;
use App\Controllers\ErrorController;
use Illuminate\Container\Container;
use Illuminate\Contracts\Container\BindingResolutionException;
use SSD\Blade\Blade;
use SSD\DotEnv\DotEnv;
use SSD\StringConverter\Factory;

/**
 * Class App
 * @package App\Utilities
 * Load the right controller
 */
class App
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Blade
     */
    private $blade;

    /**
     * App constructor.
     * @param Container $container
     * @param Blade $blade
     */
    public function __construct(Container $container, Blade $blade)
    {
        $this->container = $container;
        $this->blade = $blade;
    }

    /**
     * Call method on the controller
     */
    public function render()
    {
        try {
            $controller = $this->controller();
            $controller = new $controller($this->blade, $this->container);
            $method = $this->method($controller);

            return $controller->$method();

        } catch (\Exception $e) {
            return (new ErrorController($this->blade, $this->container))
                ->index($this->errorMessage($e));
        }
    }

    /**
     * Get controller name with namespace.
     * @return string
     * @throws BindingResolutionException
     */
    private function controller(): string
    {
        $controller = $this->container
            ->make('request')
            ->segment(1);
        if (empty($controller)) {
            $controller = 'login';
        }
        $controller = $this->controllerName($controller);

        $controller = "\\App\\Controllers\\{$controller}";

        if (!class_exists($controller)) {
            $controller = "\\App\\Controllers\\PageController";
        }

        return $controller;
    }

    /**
     * Get Controllers class name.
     * @param string $controllerName
     * @return mixed
     */
    private function controllerName(string $controllerName): string
    {
        return \SSD\StringConverter\Factory::hyphenToClassName($controllerName) . 'Controller';

    }

    /**
     * Get method name
     * @param Controller $controller
     * @return string
     * @throws BindingResolutionException
     */
    private function method(Controller $controller): string
    {
        $method = $this->container->make('request')->segment(2);
        if (empty($method)) {
            $method = 'index';
        }
        $method = $this->methodName($method);

        if (!method_exists($controller, $method)) {
            throw new \Exception("Invalid method call " . get_class($controller) . "::{$method}");
        }

        return $method;
    }

    /**
     * Get method name
     * @param string $method
     * @return mixed
     */
    private function methodName(string $method)
    {
        return Factory::hyphenToCamel($method);
    }

    /**
     * Get 404 message
     * @param \Exception $e
     * @return string
     */
    private function errorMessage(\Exception $e): string
    {
        if (!DotEnv::is('APP_ENV', 'live')) {
            return $e->getMessage();
        }

        return $this->liveErrorMessage();
    }

    /**
     * Get 404 message for live env.
     * @return string
     */
    private function liveErrorMessage(): string
    {
        return 'The page you are trying to access does not exist.';
    }

}