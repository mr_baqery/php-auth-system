<?php


namespace AppTest\Unit\Models;


use App\Models\User;
use AppTest\BaseCase;
use AppTest\Traits\Database;

class UserTest extends BaseCase
{
    use Database;

    private $properties = [
        'active' => 0,
        'name' => 'Sebastian Sulinski',
        'email' => 'info@ssdtutorials.com',
        'password' => 'secret',
        'token' => null,
        'remember_token' => null
    ];

    /**
     * Create and return new User instance.
     *
     * @param array $items
     * @return User
     */
    public function makeUser(array $items = []): User
    {
        return User::create($this->items($items));
    }

    /**
     * Merge properties.
     *
     * @param array $items
     * @return array
     */
    private function items(array $items = []): array
    {
        if (empty($items)) {
            return $this->properties;
        }

        return array_merge($this->properties, $items);
    }

    /**
     * @test
     */
    public function testCanCreateAdnFindNewUserRecord()
    {
        $this->makeUser();

        $user = User::find(1);

        $items = $this->items();

        $this->assertInstanceOf(
            User::class,
            $user,
            "User::find did not return instance of User"
        );

        $this->assertEquals(
            $items["name"],
            $user->name,
            "User::create did not add record with the correct name"
        );



    }

}