<?php


namespace App\Utilities\Validator;


use Illuminate\Support\Collection;
use SSD\StringConverter\Factory;

class Validator
{

    /**
     * @var Collection
     */
    private $rules;


    /**
     * @var Collection
     */
    private $input;

    /**
     * @var array
     */
    public $errors = [];

    /**
     * Validator constructor.
     * @param Collection $rules
     * @param Collection $input
     */
    public function __construct(Collection $rules, Collection $input)
    {
        $this->rules = $rules;
        $this->input = $input;
    }

    /**
     * Validate input
     *
     * @return bool
     */
    public function isValid(): bool
    {
        if ($this->rules->isEmpty()) {
            return true;
        }

        $this->rules->each([$this, 'validate']);

        return empty($this->errors);
    }

    /**
     * @param string $rules
     * @param string $key
     */
    public function validate(string $rules, string $key)
    {
        if (empty($rules)) {
            return;
        }

        $rules = new Collection(explode('|', $rules));

        // use() function is used  to pass argument
        $rules->each(function ($rule) use ($key) {
            $this->evaluateRule($rule, $key);
        });
    }

    /**
     * Evaluate validation rule
     *
     * @param string $rule
     * @param string $key
     */
    private function evaluateRule($rule, $key)
    {
        $parameters = null;

        $className = explode(":", $rule);

        if (count($className) > 1) {
            $parameters = $className[1];
        }

        $className = $this->validatorClass($className[0]);

        if (!(new $className($this->input, $key, $parameters))->validate()) {
            $this->errors[$key][] = $rule;
        }
    }

    /**
     * Get class name with namespace
     *
     * @param $rule
     * @return string
     */
    private function validatorClass($rule): string
    {
        return __NAMESPACE__ . "\\Rules\\" . Factory::underscoreToClassName($rule);
    }
}