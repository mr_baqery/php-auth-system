<?php


namespace App\Utilities\Mail;

use Exception;
use SSD\DotEnv\DotEnv;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class SmtpMail extends Mail
{

    public function send()
    {
        try {

            $this->validate();

            return $this->execute($this->message());

        } catch (Exception $e) {

            $this->exception = $e->getMessage();
            return 0;

        }
    }

    private function execute(\Swift_Message $message)
    {
        $mailer = Swift_Mailer::newInstance($this->transport());

        return $mailer->send($message);
    }

    private function message(): Swift_Message
    {
        $message = Swift_Message::newInstance();
        $message->setFrom($this->from)
            ->setTo($this->to)
            ->setSubject($this->subject)
            ->setBody($this->body, $this->html());

        return $message;
    }

    private function transport(): Swift_SmtpTransport
    {
        $transport = Swift_SmtpTransport::newInstance();
        $transport->setHost(DotEnv::get('MAIL_HOST'))
            ->setPort(DotEnv::get('MAIL_PORT'))
            ->setUsername(DotEnv::get('MAIL_USERNAME'))
            ->setPassword(DotEnv::get('MAIL_PASSWORD'));

        if (!empty($encryption = DotEnv::get('MAIL_ENCRYPTION'))) {
            $transport->setEncryption($encryption);
        }

        return $transport;

    }
}