<?php


namespace App\Utilities;


use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as Capsule;
use SSD\Blade\Blade;
use SSD\DotEnv\DotEnv;

class Kernel
{
    /**
     * @var Container
     */
    private $container;

    /**
     * Kernel constructor.
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;

        /** when we call following method, in anywhere
         * within our app we can call
         * Container::getInstance()->make('request')
         * and access the method of request class
         */
        Container::setInstance($this->container);


        $this->database();
    }

    /**
     * Configure the database and boot the Eloquent
     * this method is re sponsible for database configuration
     * and booting the eloquent object relational mapper
     * @return void
     */
    public function database()
    {
        $capsule = new Capsule($this->container);
        $capsule->addConnection([
            'driver' => DotEnv::get('DB_CONNECTION'),
            'host' => DotEnv::get('DB_HOST'),
            'database' => DotEnv::get('DB_DATABASE'),
            'username' => DotEnv::get('DB_USERNAME'),
            'password' => DotEnv::get('DB_PASSWORD'),
            'charset' => 'utf8',
            'collation' => 'utf8_general_ci',
            'prefix' => ''
        ]);
        /**
         * access the capsule globally via static method
         */
        $capsule->setAsGlobal();

        $capsule->bootEloquent();

        date_default_timezone_set('UTC');
    }

    /**
     * Get instance of app
     * @param string $bladeViews
     * @param string $bladeCache
     * @return App
     */
    public function make(string $bladeViews, string $bladeCache): App
    {
        /**
         * App fetches the right controller
         * does all the routing
         * outputs whatever controller outputs
         */
        return new App(
            $this->container,
            new Blade(
                $bladeViews,
                $bladeCache,
                $this->container
            ));
    }
}