<?php


namespace App\Migration;


interface TableContract
{
    /**
     * Run migration.
     * @return void
     *
     */
    public function up();

    /**
     * Rollback migration
     * @return void
     */
    public function down();
}