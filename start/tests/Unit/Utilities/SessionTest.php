<?php

namespace AppTest\Unit\Utilities;


use App\Utilities\Session\SessionManager;
use AppTest\BaseCase;

final class SessionTest extends BaseCase
{
    /**
     * @test
     */
    public function testCanSetGetSession()
    {
        SessionManager::start();

        SessionManager::set('test', 123);

        $this->assertTrue(
            SessionManager::has('test'),
            "Session:has returned false after calling Session:set"
        );


        $this->assertEquals(
            123,
            SessionManager::get('test'),
            "Session:get returned incorrect value after calling Session:set"
        );
    }

    /**
     * this test relies on the output of tagged method test
     * @test
     * @depends testCanSetGetSession
     */
    public function testCanRemoveSession()
    {
        SessionManager::remove('test');

        $this->assertFalse(
            SessionManager::has('test'),
            "Session:has returned false after calling Session:remove"
        );

        // the following method can count all items of the passed array
        $this->assertCount(
            0,
            SessionManager::all(),
            "Session:all did not return 0 record after calling Session:remove"
        );

    }

    /**
     * @test
     */
    public function testCanDestroySession()
    {
        SessionManager::start();

        SessionManager::set('test', [123, 'abc']);

        $this->assertTrue(
            SessionManager::has('test'),
            "Session:has returned false after calling Session:set with array"
        );

        $this->assertEquals(
            [123, 'abc'],
            SessionManager::get('test'),
            "Session:get returned incorrect value after calling Session::set with Array"
        );

        SessionManager::destroy();

        $this->assertCount(
            0,
            SessionManager::all(),
            "Session:all did not return 0 records after calling Session:destroy"
        );
    }
}