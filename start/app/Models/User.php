<?php


namespace App\Models;


use App\Utilities\Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;

class User extends Model
{
    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'token',
        'remember_token',
        'active'
    ];


    /**
     * set{attr}Attribute
     * if you need to modify anything before it gets saved to the database
     * create the setter with the following method
     * @param string $password
     * @return string mixed
     */
    public function setPasswordAttribute(string $password): string
    {
        // $user->password =  $this->attributes['password']
        return $this->attributes['password'] = Hash::make($password);
    }

    /**
     * Verify password
     * @param string $password
     * @return bool
     */
    public function verifyPassword(string $password): bool
    {
        return Hash::verify($password, $this->password);
    }

    /**
     * User::byEmail('test@test.com')->first()
     * why first ?  in order to fetch the record.
     * @param Builder $query
     * @param string $email
     * @return Builder
     */
    public function scopeByEmail(Builder $query, string $email): Builder
    {
        return $query->where('email', '=', $email);
    }

    /**
     * Get record by token.
     * @param Builder $query
     * @param string $token
     * @return Builder
     */
    public function scopeByToken(Builder $query, string $token): Builder
    {
        return $query->where('token', '=', $token);
    }

    /**
     * Get record by remember my token
     *
     * @param Builder $query
     * @param string $token
     * @return Builder
     */
    public function scopeByRememberToken(Builder $query, string $token): Builder
    {
        return $query->where('remember_token', '=', $token);
    }

    /**
     * Set and get token.
     *
     * @return string
     */
    public function resetToken(): string
    {
        $this->token = md5($this->email);
        $this->save();
        return $this->token;
    }

    /**
     * Check if user has active account
     * @return bool
     */
    public function isActive(): bool
    {
        return (int) $this->active === 1;
    }

    /**
     * Activate user account.
     * @return $this
     */
    public function makeActive(): User
    {
        $this->active = 1;
        $this->token = null;
        $this->save();
        return $this;
    }

    /**
     * Update password.
     *
     * @param $password
     * @return User
     */
    public function updatePassword($password): User
    {
        $this->active = 1;
        $this->token = null;
        $this->password = $password; // behind the scene we have set attr which will automatically make a hash out of it
        $this->save();

        return $this;
    }

    /**
     * Update remember me token
     *
     * @return $this
     */
    public function updateRememberToken(): User
    {
        $this->remember_token = \hash('sha256', Str::random());
        $this->save();
    }
}