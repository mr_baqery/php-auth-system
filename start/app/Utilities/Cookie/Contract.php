<?php


namespace App\Utilities\Cookie;


interface Contract
{
    /**
     * Get all cookie
     * @return array
     */
    public function all(): array;

    /**
     * Check if cookie has a given key
     * @param string $key
     * @return bool
     */
    public function has(string $key): bool;

    /**
     * Set cookie
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set(string $key, $value);

    /**
     * Get cookie
     * @param string $key
     * @return mixed
     */
    public function get(string $key);

    /**
     * @param string $key
     * @return void
     */
    public function remove(string $key);
}