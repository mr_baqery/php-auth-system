<?php


namespace App\Utilities\Mail;


use SSD\DotEnv\DotEnv;

class MailManager
{
    public static function make(array $params = [])
    {
        $className = static::className();

        return new $className($params);
    }

    private static function className(): string
    {
        $driver = DotEnv::get('MAIL_DRIVER', 'smtp');

        return __NAMESPACE__ . "\\" . ucfirst($driver) . "Mail";
    }
}